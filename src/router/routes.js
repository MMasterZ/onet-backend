const routes = [{
    path: "/",
    component: () => import("pages/login.vue"),
    name: "login"
  },
  {
    path: "/logout",
    component: () => import("pages/logout.vue"),
    name: "logout"
  },
  {
    path: "/welcomeback",
    component: () => import("pages/welcomeback.vue"),
    name: "welcomeback"
  },
  {
    path: "/studentprint",
    component: () => import("pages/student/studentprint.vue"),
    name: "studentprint"
  },
  {
    path: "/answersheetprint",
    component: () => import("pages/practice/answersheetprint.vue"),
    name: "answersheetprint"
  },
  {
    path: "/multiplechoiceprint",
    component: () => import("pages/practice/multiplechoiceprint.vue"),
    name: "multiplechoiceprint"
  },
  {
    path: "/testprint",
    component: () => import("pages/practice/testprint.vue"),
    name: "testprint"
  },
  {
    path: "/examprint",
    component: () => import("pages/practice/examprint.vue"),
    name: "examprint"
  },
  {
    path: "/question/print",
    component: () => import("pages/questionpool/questionprint.vue"),
    name: "questionprint"
  },
  {
    path: "/",
    component: () => import("layouts/MyLayout.vue"),

    children: [
      // NOTE  แบบฝึกหัด
      {
        path: "/studyplan",
        component: () => import("pages/practice/studyplan.vue"),
        name: "studyplan"
      },
      {
        path: "/practice/add",
        component: () => import("pages/practice/practiceadd.vue"),
        name: "practiceadd"
      },
      {
        path: "/practice/edit/:key/:page",
        component: () => import("pages/practice/practiceadd.vue"),
        name: "practiceedit"
      },
      {
        path: "/answersheet/:key/:page",
        component: () => import("pages/practice/answersheetmain.vue"),
        name: "answersheetmain"
      },
      {
        path: "/vdo/:key/:page",
        component: () => import("pages/practice/vdomain.vue"),
        name: "vdomain"
      },
      {
        path: "/vdo/add/:key/:page",
        component: () => import("pages/practice/vdoadd.vue"),
        name: "vdoadd"
      },
      {
        path: "/multiplechoice/:key/:page",
        component: () => import("pages/practice/multiplechoicemain.vue"),
        name: "multiplechoicemain"
      },
      {
        path: "/multiplechoice/add/:key/:page",
        component: () => import("pages/practice/multiplechoiceadd.vue"),
        name: "multiplechoiceadd"
      },
      {
        path: "/multiplechoice/edit/:key/:page/:questionKey/:type",
        component: () => import("pages/practice/multiplechoiceadd.vue"),
        name: "multiplechoiceedit"
      },
      {
        path: '/test/:key/:page',
        component: () => import('pages/practice/testmain.vue'),
        name: 'testmain'
      },
      {
        path: '/test/add/:key/:page',
        component: () => import('pages/practice/testadd.vue'),
        name: 'testadd'
      },
      {
        path: '/test/edit/:key/:page/:questionKey/:type',
        component: () => import('pages/practice/testadd.vue'),
        name: 'testedit'
      },
      {
        path: "/exam/:key/:page",
        component: () => import("pages/practice/exammain.vue"),
        name: "exammain"
      },
      // NOTE คาบเรียน
      {
        path: "/section",
        component: () => import("pages/section/sectionmain.vue"),
        name: "sectionmain"
      },
      {
        path: "/section/add",
        component: () => import("pages/section/sectionadd.vue"),
        name: "sectionadd"
      },
      {
        path: "/section/edit/:key",
        component: () => import("pages/section/sectionadd.vue"),
        name: "sectionedit"
      },
      // NOTE monitor
      {
        path: "/monitor",
        component: () => import("pages/monitor/monitormain.vue"),
        name: "monitormain"
      },
      // NOTE โรงเรียน
      {
        path: "/school",
        component: () => import("pages/school/schoolmain.vue"),
        name: "schoolmain"
      },
      {
        path: "/school/add",
        component: () => import("pages/school/schooladd.vue"),
        name: "schooladd"
      },
      {
        path: "/school/edit/:key",
        component: () => import("pages/school/schooladd.vue"),
        name: "schooledit"
      },
      // NOTE ปีการศึกษา
      {
        path: "/academic",
        component: () => import("pages/academic/academicmain.vue"),
        name: "academicmain"
      },
      {
        path: "/academic/add/:key",
        component: () => import("pages/academic/academicadd.vue"),
        name: "academicadd"
      },
      {
        path: "/academic/edit/:key",
        component: () => import("pages/academic/academicadd.vue"),
        name: "academicedit"
      },
      // NOTE คุณครู
      {
        path: "/teacher",
        component: () => import("pages/teacher/teachermain.vue"),
        name: "teachermain"
      },
      {
        path: "/teacher/add",
        component: () => import("pages/teacher/teacheradd.vue"),
        name: "teacheradd"
      },
      {
        path: "/teacher/edit/:key",
        component: () => import("pages/teacher/teacheradd.vue"),
        name: "teacheredit"
      },
      // NOTE นักเรียน
      {
        path: "/student",
        component: () => import("pages/student/studentmain.vue"),
        name: "studentmain"
      },
      {
        path: "/student/add/:schoolcode/:year",
        component: () => import("pages/student/studentadd.vue"),
        name: "studentadd"
      },
      {
        path: "/student/edit/:key",
        component: () => import("pages/student/studentadd.vue"),
        name: "studentedit"
      },
      // NOTE ผู้ใช้งาน
      {
        path: "/user",
        component: () => import("pages/user/usermain.vue"),
        name: "usermain"
      },
      {
        path: "/user/add",
        component: () => import("pages/user/useradd.vue"),
        name: "useradd"
      },
      {
        path: "/user/edit/:key",
        component: () => import("pages/user/useradd.vue"),
        name: "useredit"
      },
      {
        path: "/questionmain",
        component: () => import("pages/questionpool/questionmain.vue"),
        name: "questionmain"
      },
      {
        path: "/question/add/:key",
        component: () => import("pages/questionpool/questionadd.vue"),
        name: "questionadd"
      },
      {
        path: '/question/edit/:key/:questionKey/:type',
        component: () => import('pages/questionpool/questionadd.vue'),
        name: 'questionedit'
      },


    ]
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}

export default routes;
